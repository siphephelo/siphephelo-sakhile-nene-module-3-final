import 'dart:html';

import 'package:flutter/material.dart';

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: const Text(
          "Hello: Welcome To My App",
          textAlign: TextAlign.center,
        ));
  }
}
